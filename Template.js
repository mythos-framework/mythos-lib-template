"use strict";

var Template = function (options) {
	this._pageTitle      = '';
	this._titleSeparator = options.title_separator;
	this._siteTitle      = options.site_title;
	this._req            = null;
	this._res            = null;
};

Template.prototype.bindMiddleware = function(req, res) {
	this._req = req;
	this._res = res;
};

Template.prototype.setPageTitle = function(pageTitle) {
	this._pageTitle = pageTitle;
};

Template.prototype.setSiteTitle = function(siteTitle) {
	this._siteTitle = siteTitle;
};

Template.prototype.setTitleSeparator = function(separator) {
	this._titleSeparator = separator;
};

Template.prototype.getTitle = function() {
	if (this._pageTitle !== '') {
		return this._pageTitle + ' ' + this._titleSeparator + ' ' + this._siteTitle;
	} else {
		return this._siteTitle;
	}
};

Template.prototype.getPageTitle = function() {
	return this._pageTitle;
};

Template.prototype.getSiteTitle = function() {
	return this._siteTitle;
};

Template.prototype.render = function(path, params) {
	this._res.render('partials/' + path, params);
};

module.exports = Template;
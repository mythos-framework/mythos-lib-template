# mythos-lib-moment

Mythos library wrapper for [moment](http://momentjs.com)

## Installation

```bash
npm install mythos-lib-moment
```

## Usage

### Loading

#### Autoloading

Open `config/autoload.js` and add the library.

```js
module.exports = {
	...
	"lib": {
		...
		"moment": true,
		...
	},
	...
};
```

#### Manual Loading

```js
mythos.load('lib', 'moment');
```

### Calling the Library
```js
var currentDate = mythos.lib.moment();
```

## License

Copyright (c) 2014, Hyubs Ursua <hyubs.u@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.